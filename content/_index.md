---
title: "Research @ Eclipse Foundation"
headline: "Research @ Eclipse Foundation"
hide_sidebar: true
show_featured_story: false
date: 2019-09-10T15:50:25-04:00
hide_page_title: true
container: "container-fluid"
#hide_breadcrumbs: true
show_featured_story: false
show_featured_footer: false
layout: single
#links: [[href: "https://events.eclipse.org/2020/sam-iot/sam-iot-cfp.pdf", text: "Download call for papers"]]
header_wrapper_class: "header-default-bg-img small-jumbotron-subtitle"
jumbotron_class: "col-sm-24"
custom_jumbotron_class: "col-sm-24"
custom_jumbotron: |
    <div class="home-custom-jumbotron row">
        <p class="col-sm-14">
            Business-friendly open source and open innovation underpin exploitation, community building, and dissemination strategies for European projects
        </p>
        <div class="research-logo-wrapper col-sm-10">
            <img src="images/eclipse-research-logo-transparent.svg" class="research-logo" alt="Eclipse Research Logo" />
        </div>
    </div>
---

{{< home-section-header >}}

{{< home-section-info >}}

{{< home-section-opportunities >}}

{{< home-section-organizations >}}

{{< grid/section-container class="row-gray padding-top-40 padding-bottom-40 margin-bottom-40" isMarkdown="false" >}}
    <h2 class="text-center margin-bottom-40">The Eclipse Foundation is a Partner in these Projects</h2>
    {{< eclipsefdn_projects is_static_source="true" url="/projects/index.json" templateId="tpl-projects-item-research" display_view_more="false" >}}
{{</ grid/section-container >}}

{{< newsroom-section working_group="research" class="margin-bottom-30" resource_class="col-md-5 newsroom-resource-card newsroom-resource-card-match-height match-height-item" >}}

{{< home-section-contact >}}

{{< mustache_js template-id="tpl-projects-item-research" path="/js/templates/tpl-projects-item-research.mustache" >}}
