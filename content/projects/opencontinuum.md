---
title: "Open Continuum"
date: 2022-09-01T00:00:00-00:00
project_logo: "images/research/r-projects/opencontinuum.png"
tags: ["Computer architecture", "pervasive computing", "ubiquitous computing", "IoT", "System of Systems", "Open Source"]
homepage: ""
facebook: ""
linkedin: ""
twitter: ""
youtube: ""
funding_bodies: ["horizon2020"]
eclipse_projects: []
project_topic: "Cloud-Edge (CSA)"
summary: "An Open Ecosystem for European strategic autonomy and interoperability across the computing continuum industry"
hide_page_title: true
hide_sidebar: true
container: "bg-white"
header_wrapper_class: "header-projects-bg-img"
description: "# **Open Continuum**

OpenContinuum addresses the coordination and support of the Cloud-Edge-IoT domain, with a specific thematic focus on the supply-side of the computing continuum landscape.


An integrated, open ecosystem built around Open Source, Open Standards, and the effective blending of the two vibrant European communities of Cloud Computing and Internet of Things (IoT) is the key enabler for European prosperity in the Cloud-Edge-IoT domain and more widely in the data economy. Therefore the core ambition of OpenContinuum is fostering European strategic autonomy and interoperability through an open ecosystem for the computing continuum. Such an ecosystem will contain the research and innovation projects in the Cloud-Edge-IoT portfolio to be coordinated, the diverse community evolved from the current Cloud and IoT ones, with the addition of further actors, initiatives, and alliances of significance. The supply-side nature of OpenContinuum's agenda will orient the themes and focus of the project activities, but will not limit the scope of the community building. The active landscaping and engagement work of the project will be key to bring the Cloud and IoT communities together and to express all points of view with a common language and understanding. OpenContinuum will then provide strong guidance and support to European computing continuum actors to contribute to and lead open-source projects and standardisation efforts. 

Key project contributions to the relevant expected outcomes include: a baseline common open architecture for computing continuum research projects, reinforced collaboration between European public and private initiatives from Cloud to edge to IoT, and increased awareness on the importance of Open Source and standards for EU digital autonomy. These contributions, arising from project results, will evolve further towards larger-scale impacts and strategic objectives for European technological, economic, and societal advancement.


This project is running from January 2021 - December 2022."
---

{{< grid/div class="bg-light-gray" isMarkdown="false">}}
{{< grid/div class="container research-page-section" >}}

# Consortium
* National Technical University of Athens, Greece
* Consorzio Nazionale Interuniversitario per le Telecomunicazioni, Italy
* SIEMENS AKTIENGESELLSCHAFT, Germany
* ATOS IT, Spain
* Institut National de Recherche en Informatique et Automatique, France
* University of Macedonia, Greece
* Fundingbox Accelerator Sp. Z o.o., Polland
* Odin Solutions SL, Spain
* ZURCHER HOCHSCHULE FUR ANGEWANDTE WISSENSCHAFTEN, Swisszerland
* Alter Way, France
* Internet Institute Ltd, Slovenia
* ECLIPSE FOUNDATION EUROPE GMBH, Germany
* WINGS ICT SOLUTIONS INFORMATION & COMMUNICATION TECHNOLOGIES IKE, Greece
* IBM IRELAND LIMITED, Ireland
* ESAOTE SPA, Italy
* LUKA KOPER, PORT AND LOGISTIC SYSTEM, D.D., Slovenia
* W3C/ERCIM, France

{{</ grid/div>}}
{{</ grid/div>}}
